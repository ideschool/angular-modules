import {Routes} from '@angular/router';

export const routes: Routes = [
  {
    path: 'admin',
    loadChildren: './admin/admin.module#AdminModule',
  },
  {
    path: 'user',
    loadChildren: './user/user.module#UserModule',
  },
  {
    path: '',
    loadChildren: './landing/landing.module#LandingModule',
  },
  {
    path: '**',
    redirectTo: '',
  },
];
