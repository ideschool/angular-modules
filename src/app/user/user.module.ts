import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { MainComponent } from './pages/main/main.component';
import { NextPageComponent } from './pages/next-page/next-page.component';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    ComponentsModule,
  ],
  declarations: [MainComponent, NextPageComponent]
})
export class UserModule { }
