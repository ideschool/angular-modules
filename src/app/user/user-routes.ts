import {Routes} from '@angular/router';
import {MainComponent} from './pages/main/main.component';
import {NextPageComponent} from './pages/next-page/next-page.component';

export const routes: Routes = [
  {
    path: '',
    component: MainComponent,
  },
  {
    path: 'next-page',
    component: NextPageComponent,
  },
  {
    path: '**',
    redirectTo: '',
  },
];
