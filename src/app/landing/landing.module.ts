import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingRoutingModule } from './landing-routing.module';
import { MainComponent } from './pages/main/main.component';
import { PricesComponent } from './pages/prices/prices.component';

@NgModule({
  imports: [
    CommonModule,
    LandingRoutingModule
  ],
  declarations: [MainComponent, PricesComponent]
})
export class LandingModule { }
