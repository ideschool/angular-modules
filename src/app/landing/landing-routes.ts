import {Routes} from '@angular/router';
import {MainComponent} from './pages/main/main.component';
import {PricesComponent} from './pages/prices/prices.component';

export const routes: Routes = [
  {
    path: '',
    component: MainComponent,
  },
  {
    path: 'prices',
    component: PricesComponent
  },
  {
    path: '**',
    redirectTo: '',
  },
];
