import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputComponent } from './input/input.component';
import { CheckboxComponent } from './checkbox/checkbox.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [InputComponent, CheckboxComponent],
  exports: [InputComponent, CheckboxComponent]
})
export class ComponentsModule { }
