import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { MainComponent } from './pages/main/main.component';
import { UsersListComponent } from './pages/users-list/users-list.component';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    ComponentsModule
  ],
  declarations: [MainComponent, UsersListComponent]
})
export class AdminModule { }
