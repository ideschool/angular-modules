import { Routes } from '@angular/router';
import { MainComponent } from './pages/main/main.component';
import {UsersListComponent} from './pages/users-list/users-list.component';

export const routes: Routes = [
  {
    path: '',
    component: MainComponent
  },
  {
    path: 'users',
    component: UsersListComponent
  },
  {
    path: '**',
    redirectTo: '',
  },
];
